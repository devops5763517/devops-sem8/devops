package com.example.project.service;

import java.util.List;

import com.example.project.dto.Task;
import com.example.project.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService{

    @Autowired
    private TaskRepository repository;
    @Override
    public List<Task> getTask() {
        return repository.findAll();
    }
}
