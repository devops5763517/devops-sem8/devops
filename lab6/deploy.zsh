#!/bin/zsh

set -xe

INIT=$(pwd)

# Запуск
PROJECTS=('db' 'project' 'front' 'nginx')
for project in "${PROJECTS[@]}"; do
    cd "$project"
    docker-compose up -d
    cd "$INIT"
done
