#!/bin/zsh

set -x
set -e

docker build \
    --target system \
    --cache-from lab6:system \
    -t lab6:system \
    -f Dockerfile \
    .

docker build \
    --target build \
    --cache-from lab6:system \
    --cache-from lab6:build \
    -t lab6:build \
    -f Dockerfile \
    .

docker build \
    --target app \
    --cache-from lab6:build \
    -t lab6:app \
    -f Dockerfile \
    .
