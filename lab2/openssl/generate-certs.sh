#!/bin/sh

while true; do
  openssl req -x509 -newkey rsa:2048 -nodes -keyout /certs/key.pem -out /certs/cert.pem -subj "/C=GB/L=London/O=Any Corp Ltd/CN=localhost"
  sleep 86400
done
