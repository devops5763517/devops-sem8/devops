package com.example.project.service;

import java.util.List;

import com.example.project.dto.Task;

public interface TaskService {
    List<Task> getTask();
}
